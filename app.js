var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var net = require('net')

var routes = require('./routes/index');
var config = require('./config/config').config;

var app = express();
var Mediator = require('./lib/mediator').Mediator;
var mediator = new Mediator();
var ClientHandler = require('./lib/clienthandler').ClientHandler;
var clientHandler = new ClientHandler(mediator, config.serverPass, config.restrictedData);

app.set('ioReady', function() {
  var io = app.get('io');
  clientHandler.ioReady(io);
});

// Connect to 7 days to die server
var telnet = require('telnet-client');
var connection = new telnet();

var ServerHandler = require('./lib/serverhandler').ServerHandler;
var serverHandler = new ServerHandler(config.telnet, config.display || {}, mediator);
serverHandler.start();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/bootstrap', express.static(path.join(__dirname, 'node_modules/bootstrap/dist')));

app.use('/', routes);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

module.exports = app;

/**
 * Created by sean on 7/11/2015.
 */
var Mediator = function() {
    this.topics = {};
};

Mediator.prototype.subscribe = function (topic, callback) {
    if (!this.topics.hasOwnProperty(topic)) {
        this.topics[topic] = [];
    }
    this.topics[topic].push(callback);
    return true;
};

Mediator.prototype.unsubscribe = function(topic, callback) {
    if (!this.topics.hasOwnProperty(topic)) {
        return false;
    }

    for (var i = 0, len = this.topics.length; i < len; i++) {
        if (this.topics[topic][i] === callback) {
            this.topics[topic].splice(i, 1);
            return true;
        }
    }

    return false;
};

Mediator.prototype.publish = function () {
    var args = Array.prototype.slice.call(arguments);
    var topic = args.shift();

    if (!this.topics.hasOwnProperty(topic)) {
        return false;
    }

    for (var i = 0, len = this.topics[topic].length; i < len; i++) {
        this.topics[topic][i].apply(undefined, args);
    }

    return true;
};

exports.Mediator = Mediator;
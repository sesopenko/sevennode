/**
 * Created by sean on 7/11/2015.
 */

var telnet = require('telnet-client');

/**
 * Helper class for displaying info on the server time
 *
 * @param timeString
 *   Time string as received from the server
 * @constructor
 */
var ServerTime = function (timeString) {
    this.timeString = timeString;
};

/**
 * Get what is to be displayed to the player regarding the game time.
 *
 * @returns {{currentTime: string, nextBloodMoon: string}}
 */
ServerTime.prototype.getDisplay = function() {
    var display = {
        'currentTime': this.timeString,
        'nextBloodMoon': 'unknown',
        isBloodMoon: false
    };
    var matches = this.timeString.toString().match(/Day (\d+), * (\d{2,2}:\d{2,2})/);
    if (matches) {
        var thisDay = parseInt(matches[1]);
        var numDays = 7 - (thisDay % 7);
        display.nextBloodMoon = 'The next blood moon will be on day ' + (thisDay + numDays).toString();
        if (numDays == 6) {
            // blood moon doesn't happen on day 1
            if (thisDay > 1) {
                display.nextBloodMoon = 'Today is a blood moon';
                display.isBloodMoon = true;
            }
        }
    }
    return display;
};

var PlayerStats = function(line) {
    var playerRegex = /(\d+)\. *id=(\d+), *([^,]+), *pos=\(([-\d\.]+), *([-\d\.]+), *([-\d\.]+)\), *rot=\(([-\d\.]+), *([-\d\.]+), *([-\d\.]+)\), *remote=([^,]+), *health=(\d+), *deaths=(\d+), *zombies=(\d+), *players=(\d+), *score=(\d+), *level=(\d+), *steamid=(\d+), *ip=(\d+\.\d+\.\d+\.\d+), *ping=(\d+)/i;
    this.matches = line.toString().match(playerRegex);
};

PlayerStats.prototype.getStats = function() {
    if (this.matches) {
        var stats = {
            id: this.matches[2],
            name: this.matches[3],
            pos: {
                x: this.matches[4],
                y: this.matches[5],
                z: this.matches[6]
            },
            rot: {
                x: this.matches[7],
                y: this.matches[8],
                z: this.matches[9]
            },
            remote: this.matches[10],
            health: this.matches[11],
            deaths: this.matches[12],
            zombies: this.matches[13],
            players: this.matches[14],
            score: this.matches[15],
            level: this.matches[16],
            steamid: this.matches[17],
            ip: this.matches[18],
            ping: this.matches[19]
        };
        return stats;
    }
};

var KnownPlayerStats = function(line) {
    var regex = /(\d+)\. *([^,]+), *id=(\d+), *steamid=(\d+), *online=(False|True), *ip=(\d+\.\d+\.\d+\.\d+), *playtime=(\d+ m), *seen=(\d{4,4}-\d{2,2}-\d{2,2} *\d{2,2}:\d{2,2})+/i;
    this.matches = line.toString().match(regex);
};

KnownPlayerStats.prototype.getStats = function() {
    if (this.matches) {
        var stats = {
            name: this.matches[2],
            id: this.matches[3],
            steamid: this.matches[4],
            online: this.matches[5],
            ip: this.matches[6],
            playtime: this.matches[7],
            seen: this.matches[8]
        };
        return stats;
    }
}

var Player = function(steamid) {
    this.steamid = steamid;
    this.stats = null;
    this.lastSeen = new Date();
};

Player.prototype.updateStats = function(stats) {
    this.stats = stats;
    this.lastSeen = new Date();
};

/**
 * Get only the fields required
 *
 * @param {array} display
 *   Fields to return
 * @returns {object}
 *   Object containing the filtered fields
 */
Player.prototype.getCleaned = function(display) {
    if (display === 'ALL') {
        return this.stats;
    }
    else {
        var cleaned = {};
        for (var i = 0, len = display.length; i < len; i++) {
            var value = display[i];
            if (this.stats.hasOwnProperty(value)) {
                cleaned[value] = this.stats[value];
            }
        }
        return cleaned;
    }
};

var PlayerList = function(display) {
    // Seconds until considered dropped.
    this.display = display;
    this.consideredDropped = 15 * 1000;
    this.players = {};
};

PlayerList.prototype.updatePlayer = function(stats) {
    var steamid = stats.steamid;
    if (!this.players.hasOwnProperty(steamid)) {
        // Create a new player object
        var player = new Player(steamid);
        player.updateStats(stats);
        this.players[player.steamid] = player;
    }
    else {
        this.players[steamid].updateStats(stats);
    }
};

PlayerList.prototype.cleanup = function() {
    var now = new Date();
    for (steamid in this.players) {
        if (this.players.hasOwnProperty(steamid)) {
            var player = this.players[steamid];
            var difference = now - player.lastSeen;
            if (difference > this.consideredDropped) {
                delete(this.players[steamid]);
            }
        }
    }
};

PlayerList.prototype.getList = function() {
    this.cleanup();
    var filtered = [];
    for (var id in this.players) {
        if (this.players.hasOwnProperty(id)) {
            var player = this.players[id];
            var cleaned = player.getCleaned(this.display);
            filtered.push(cleaned);
        }
    }
    return filtered;
}

var ServerHandler = function(telnetConfig, display, mediator) {
    this.telnetConfig = telnetConfig;
    this.display = display || {};
    this.mediator = mediator;

    this.connection = null;
    this.newPlayerList();
    this.newKnownPlayerList();

    this.secondsPerCommand = 2;
    this.commands = [
        'lp',
        'lkp',
        'gettime'
    ];

    this.currentCommand = null;
};

ServerHandler.prototype.start = function() {
    if (!this.connection) {
        console.log('starting connection');
        this.connect();
    }
};

/**
 * Creates a new player list
 */
ServerHandler.prototype.newPlayerList = function() {
    var display = 'ALL';
    if (this.hasOwnProperty('display') && this.display.hasOwnProperty('playerStats')) {
        display = this.display.playerStats;
    }
    this.playerList = new PlayerList(display);
};

/**
 * Creates a new known player list.
 */
ServerHandler.prototype.newKnownPlayerList = function() {
    var display = 'ALL';
    if (this.hasOwnProperty('display') && this.display.hasOwnProperty('knownPlayerList')) {
        display = this.display.knownPlayerList;
    }
    this.knownPlayerList = new PlayerList(display);
}

ServerHandler.prototype.connect = function() {
    var self = this;
    self.connection = new telnet();

    // Refresh the player list because it may be out of date
    self.newPlayerList();

    self.connection.on('timeout', function() {
        console.log('connection timed out to telnet server.  ending');
        self.connection.end();
    });

    self.connection.on('error', function(e) {
        console.log('got a telnet error: ', e);
    });

    self.connection.on('connect', function() {
        console.log('connection to telnet server established');
    });

    self.connection.on('close', function() {
        console.log('Connection closed.  Retrying a connection in 5 seconds');
        self.connection = null;
        // Retry a connection in 5 seconds.
        setTimeout(function() {
            self.connect();
        }, 5 * 1000);
    });

    self.connection.on('ready', function(prompt) {
        console.log('telnet connection ready');
        self.executeCommands();
    });

    var telnetParams = {
        host: self.telnetConfig.host,
        port: self.telnetConfig.port,
        shellPrompt: /\r\n$/,
        timeout: 15 * 1000,
        passwordPrompt: /Please enter password:/i,
        username: '',
        password: self.telnetConfig.password
    };

    self.connection.connect(telnetParams);
};

ServerHandler.prototype.executeCommands = function() {
    var self = this;
    // Current index of the commands
    var cmdId = (self.currentCommand || 0) % self.commands.length;
    var currentCommand = self.commands[cmdId];
    self.connection.exec(currentCommand, function(response){
        self.handleResponse(response);
        // Increment the command so the next one is executed
        self.currentCommand = (cmdId + 1) % self.commands.length;
        setTimeout(function() {
            self.executeCommands();
        }, self.secondsPerCommand * 1000);
    });
};

ServerHandler.prototype.handleResponse = function(response) {
    var self = this;
    var lines = response.toString().split('\n');
    var gotPlayerList = false;
    var gotKnownPlayerList = false;
    for( var i = 0, numLines = lines.length; i < numLines; i++) {
        var line = lines[i];

        // First see if it's a response to 'lp'
        var playerStats = new PlayerStats(line);
        var stats = playerStats.getStats();
        if (stats) {
            self.playerList.updatePlayer(stats);
            gotPlayerList = true;
            continue;
        }

        var knownStats = new KnownPlayerStats(line);
        var stats = knownStats.getStats();
        if (stats) {
            self.knownPlayerList.updatePlayer(stats);
            gotKnownPlayerList = true;
            continue;
        }

        var dayMatch = line.toString().match(/(Day \d+, *\d{2,2}:\d{2,2})/i);
        if (dayMatch) {
            var serverTime = new ServerTime(line.toString());

            self.mediator.publish('time', serverTime.getDisplay());
            continue;
        }
        var chatMessage = self.getChat(line);
        if (chatMessage) {
            self.mediator.publish('chat', chatMessage);
            continue;
        }
    }

    if (gotPlayerList) {
        self.mediator.publish('playerStats', self.playerList.getList());
    }
    if (gotKnownPlayerList) {
        self.mediator.publish('knownPlayerList', self.knownPlayerList.getList());
    }
};

/**
 * Get a chat message object if it was a chat message.
 *
 * @param response
 * @returns {*|object}
 *   If it wasn't a message then false.  If it was a message then a message object.
 */
ServerHandler.prototype.getChat = function(response) {
    // Example:
    // 2015-07-12T21:28:54 32121.289 INF GMSG: speshuled420: sup
    var matches = response.toString().match(/(\d{4,4}-\d{2,2}-\d{2,2}T\d{1,2}\:\d{1,2}\:\d{1,2}) +\d+\.\d+ +INF GMSG: +([^:]+): ([^\n]*)/);
    if (matches) {
        var chatMessage = {
            'time': matches[1],
            'userName': matches[2],
            'message': matches[3]
        }
        return chatMessage;
    }
    return false;
}

exports.ServerHandler = ServerHandler;
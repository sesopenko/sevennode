/**
 * Created by sean on 7/11/2015.
 */
var crypto = require('crypto');

var ClientHandler = function(mediator, serverPass, restrictedData) {
    var self = this;
    this.io = null;
    this.mediator = mediator;
    // client sends hashed values.  Create the hash now so it's not recalculated on every authentication request.
    var shasum = crypto.createHash('sha256');
    shasum.update(serverPass);
    this.serverPass = shasum.digest('hex');
    this.restrictedData = restrictedData;

    this.clientSockets = [];

    function onPlayerStats(players) {
        sendMessage('playerStats', players);
    }

    function onKnownPlayerList(players) {
        sendMessage('knownPlayerList', players);
    }

    function onTime(time) {
        sendMessage('time', time);
    }

    /**
     * Callback for when a chat message is received from the mediator.
     * @param {object} chatMessage
     *   Chat message object
     */
    function onChat(chatMessage) {
        sendMessage('chat', chatMessage);
    }

    /**
     * Sends messages to the correct clients based on authentication.
     *
     * @param {string} type
     *   Message type
     * @param {*} message
     *   Message to send.
     */
    function sendMessage(type, message) {
        if (self.io) {
            if (self.restrictedData.indexOf(type) > -1) {
                // This is restricted, send to the authenticated room
                self.io.to('authenticated').emit(type, message);
            }
            else {
                self.io.sockets.emit(type, message);
            }
        }
    }

    mediator.subscribe('playerStats', onPlayerStats);
    mediator.subscribe('knownPlayerList', onKnownPlayerList);
    mediator.subscribe('time', onTime);
    mediator.subscribe('chat', onChat);
};

ClientHandler.prototype.ioReady = function(io) {
    var self = this;
    self.io = io;
    self.io.on('connection', function(socket) {
        // Add client to list of sockets
        socket.on('disconnect', function() {
            // Client disconnected
        });
        socket.on('authenticate', function(password) {
            if (password == self.serverPass) {
                socket.join('authenticated');
                socket.emit('authenticated');
            }
            else {
                socket.emit('invalidPass');
            }
        })
    });
};

exports.ClientHandler = ClientHandler;
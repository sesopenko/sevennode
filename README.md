# SevenNode Server Stats #

SevenNode is a server stats application that lets a user view real-time server stats for a 7 Days to Die server.  It is powered
by node.js and uses telnet to collect statistics.  Data is fed to the client in real-time using websockets.

See an example at [home.seanesopenko.ca:3000](http://home.seanesopenko.ca:3000)

## License ##

SevenNode is licensed under the [GNU GPLv3 License](http://www.gnu.org/licenses/gpl-3.0.en.html).

## How do I get set up? ##

Running on Ubuntu Server 14.04

* `sudo apt-get update && sudo apt-get install nodejs npm`
* clone the project (must be a bitbucket user and configured your git client):  `git clone git@bitbucket.org:sesopenko/sevennode.git`
* Perform an `npm install` to install the dependencies into the node_modules directory
* Copy `config/config.example.js` to `config/config.js` then change the settings to match your own needs
* If your 7 Days to Die server is configured to not use a telnet password then leave it as an empty string
* execute `nodejs ./bin/www` (if you installed node.js on windows then `node bin\www`)

## Contribution guidelines ##

If you'd like to make contributions fork the project then make a pull request.

## How do I report issues or suggest features? ##

Submit a support ticket if you're having problems or have an idea for a feature.  I don't have the time to help with
general server administration questions.  If you are having problems installing node.js or any other requisite packages
then I recommend you consult with the online community for your OS.

I've tested SevenNode on Windows 7 and Ubuntu 14.04 Server x64.
/**
 * Created by sean on 7/11/2015.
 */

/*
Copy this file to config.js
 */
exports.config = {
    telnet: {
        host: '127.0.0.1',
        password: '', // leave blank if no password set,
        port: 8081
    },
    display: {
        playerStats: [
            // available in 12.1:  name, id, pos, rot, remote, health, deaths, zombies, players, score, level, steamid, ip, ping
            'name', 'health', 'deaths', 'zombies', 'ping'
        ],
        knownPlayerList: [
            // available in 12.1: name, id, steamid, online, ip, playtime, seen
            'name', 'online', 'playtime', 'seen'
        ]
    },
    /*
     This is the password users will have to enter to gain access to some data.
      */
    serverPass: 'somethingsecret',
    // Possible data to restrict:
    // chat, playerStats, knownPlayerList, time
    restrictedData: ['chat']
};